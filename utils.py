import logging
import random
import string
import sys

__author__ = 'fomars'


def random_string_generator(length, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(length))


class Logger(object):
    """
   Fake file-like stream object that redirects writes to a logger instance.
   """

    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())


def set_logging():
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
        filename="gitbot.log",
        filemode='a'
    )

    stderr_logger = logging.getLogger('STDERR')
    sl = Logger(stderr_logger, logging.ERROR)
    sys.stderr = sl
