__author__ = 'fomars'

REPO_URL = 'git@gitbot:traqswdemo/demo-web-app.git'
REPO_NAME = 'demoWebApp'
GIT_USERNAME = 'developer1_traq'
GIT_EMAIL = 'bill.echlin@traqsoftware.co.uk'
FILE_TO_EDIT = 'foo.code'
JIRA_TICKET = 'DEMO-30'
CLEAN_UP = False